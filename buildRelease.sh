#bin/bash

g++ -O3 -std=c++17 '-DSDL_MAIN_HANDLED -DGLEW_STATIC' main.cpp VertexBuffer.cpp -o out/opengl -lGL -lSDL2 -lGLEW
