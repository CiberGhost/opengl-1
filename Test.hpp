#include "Typedefs.hpp"
#include "VertexBuffer.hpp"
#include "OpenGlWindow.hpp"

#include <iostream>

void withoutVAO(uint16 countLoop = 3, uint16 drawLoop = 10000)
{
	std::cout << "withoutVAO:\ncountLoop: " << countLoop << "\ndrawLoop: " << drawLoop << std::endl << std::endl;

	float64 start = clock();

	float64 elapsed = 0.0, elapsedPlus = 0.0;

	uint64 count = 0;

	uint16 widht = 800;
	uint16 height = 600;

	std::string title{"C++ OpenGL"};

	bool close = false;

	SDL_Event evt{};

	Vertex vertices[] =
		{
			Vertex{-0.5f, -0.5f, 0.0f},
			Vertex{0.0f, 0.5f, 0.0f},
			Vertex{0.5f, -0.5f, 0.0f}
		};
	uint16 numVertices = 3;

	OpenGlWindow window{title, widht, height};

	VertexBuffer vertexBuffer{vertices, numVertices, true};

	std::cout << "init time: " << (clock() - start) / 1000 << "ms\n" << std::endl;

	for (size_t j = 0; j < countLoop; ++j)
	{
		for (size_t i = 0; i < drawLoop; ++i)
		{
			start = clock();
			window.clear();

			window.drawTriangle(vertexBuffer, 0, numVertices, true);

			window.swap();

			elapsed += clock() - start;

			while (window.pollEvent(&evt))
			{
				switch (evt.type)
				{
				case (SDL_QUIT):
				{
					close = true;
					break;
				}
				default:
					break;
				}
			}
		}

		++count;
		
		std::cout << "countLoop round: " << count << ":\n";
		std::cout << "everage time: " << elapsed / (1000 * drawLoop) << "ms\n\n";

		elapsedPlus += elapsed;

		elapsed = 0.0;
	}

	std::cout << "total everage time: " << elapsedPlus / (1000000 * countLoop) << "ms\n\n\n";
}

void withVAO(uint16 countLoop = 3, uint16 drawLoop = 10000)
{
	std::cout << "withVAO:\ncountLoop: " << countLoop << "\ndrawLoop: " << drawLoop << std::endl << std::endl;

	float64 start = clock();

	float64 elapsed = 0.0, elapsedPlus = 0.0;

	uint64 count = 0;

	uint16 widht = 800;
	uint16 height = 600;

	std::string title{"C++ OpenGL"};

	bool close = false;

	SDL_Event evt{};

	Vertex vertices[] =
		{
			Vertex{-0.5f, -0.5f, 0.0f},
			Vertex{0.0f, 0.5f, 0.0f},
			Vertex{0.5f, -0.5f, 0.0f}};
	uint16 numVertices = 3;

	OpenGlWindow window{title, widht, height};

	VertexBuffer vertexBuffer{vertices, numVertices};

	std::cout << "init time: " << (clock() - start) / 1000 << "ms\n" << std::endl;

	for (size_t i = 0; i < countLoop; ++i)
	{
		for (size_t j = 0; j < drawLoop; ++j)
		{
			start = clock();
			window.clear();

			window.drawTriangle(vertexBuffer, 0, numVertices);

			window.swap();

			elapsed += clock() - start;

			while (window.pollEvent(&evt))
			{
				switch (evt.type)
				{
				case (SDL_QUIT):
				{
					close = true;
					break;
				}
				default:
					break;
				}
			}
		}

		++count;

		std::cout << "countLoop round: " << count << ":\n";
		std::cout << "everage time: " << elapsed / (1000 * drawLoop) << "ms\n\n";

		elapsedPlus += elapsed;

		elapsed = 0.0;
	}

	std::cout << "total everage time: " << elapsedPlus / (1000000 * countLoop) << "ms\n";
}