#include "VertexBuffer.hpp"


VertexBuffer::VertexBuffer (void *data, uint32 numVertices)
{
	glGenVertexArrays (1, &vao);
	glBindVertexArray (vao);

	glGenBuffers(1, &bufferID);
	glBindBuffer (GL_ARRAY_BUFFER, bufferID);
	glBufferData (GL_ARRAY_BUFFER, numVertices * sizeof (Vertex), data, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), offsetof (class Vertex, x));

	glBindVertexArray (0);
}

VertexBuffer::VertexBuffer (void *data, uint32 numVertices, bool withoutVAO)
{
	glGenBuffers(1, &bufferID);
	glBindBuffer (GL_ARRAY_BUFFER, bufferID);
	glBufferData (GL_ARRAY_BUFFER, numVertices * sizeof (Vertex), data, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), offsetof (class Vertex, x));
}

VertexBuffer::~VertexBuffer ()
{
	glDeleteBuffers(1, &bufferID);
}

void VertexBuffer::bind ()
{
	glBindVertexArray (vao);
}

void VertexBuffer::bind (bool withoutVAO)
{
	glBindBuffer (GL_ARRAY_BUFFER, bufferID);
	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), offsetof (class Vertex, x));
}

void VertexBuffer::unbind ()
{
	glBindVertexArray (0);
}

void VertexBuffer::unbind (bool withoutVAO)
{
	glBindBuffer (GL_ARRAY_BUFFER, 0);
}