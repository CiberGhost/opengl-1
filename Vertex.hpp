#pragma once

#include "Typedefs.hpp"


class Vertex
{
public:
	float32 x;
	float32 y;
	float32 z;
};