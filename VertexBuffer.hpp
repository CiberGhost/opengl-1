#pragma once

#include <GL/glew.h>

#include "Typedefs.hpp"
#include "Vertex.hpp"


class VertexBuffer
{
private:
	GLuint bufferID;
	GLuint vao;

public:
	VertexBuffer (void *data, uint32 numVertices);
	VertexBuffer (void *data, uint32 numVertices, bool withoutVAO);
	virtual ~VertexBuffer ();

	void bind ();
	void bind (bool withoutVAO);
	void unbind ();
	void unbind (bool withoutVAO);
};