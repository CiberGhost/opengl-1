#pragma once

#include "Typedefs.hpp"
#include "VertexBuffer.hpp"

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include <string>


class OpenGlWindow
{
public:
	OpenGlWindow (std::string &windowTitle, uint16 widht, uint16 height);
	~OpenGlWindow ();

	inline void clear () {glClear (GL_COLOR_BUFFER_BIT);}
	inline void swap () {SDL_GL_SwapWindow (window);}
	inline int pollEvent (SDL_Event *evt) {return SDL_PollEvent (evt);}

	inline void drawTriangle (VertexBuffer &vb, uint8 first, uint16 count) {vb.bind (); glDrawArrays (GL_TRIANGLES, first, count); vb.unbind ();}
	inline void drawTriangle (VertexBuffer &vb, uint8 first, uint16 count, bool withoutVAO) {vb.bind (withoutVAO); glDrawArrays (GL_TRIANGLES, first, count); vb.unbind (withoutVAO);}

private:
	SDL_Window *window;
	SDL_GLContext glContext;
};