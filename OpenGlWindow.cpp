#include "OpenGlWindow.hpp"

#include <iostream>
#include <stdexcept>


OpenGlWindow::OpenGlWindow (std::string &windowTitle, uint16 widht, uint16 height)
{
	if (SDL_Init (SDL_INIT_EVERYTHING))
	{
		std::cerr << "Failed to init SDL\n";
		throw std::runtime_error ("Failed to init SDL");
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	window = SDL_CreateWindow (windowTitle.c_str (), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, widht, height, SDL_WINDOW_OPENGL);

	if (window == nullptr)
	{
		std::cerr << "Failed to create SDL window\n";
		throw std::runtime_error ("Failed to create SDL window");
	}

	glContext = SDL_GL_CreateContext (window);

	GLenum err = glewInit ();
	if (err != GLEW_OK)
	{
		std::cerr << "Failed to init Glew\n";
		std::cerr << "Error: " << glewGetErrorString (err) << std::endl;
		throw std::runtime_error ("Failed to init Glew");    
	}

	glClearColor (0, 0, 0, 1);
}

OpenGlWindow::~OpenGlWindow ()
{
	SDL_DestroyWindow (window);
	
	SDL_Quit ();
}